const reset = document.querySelector('.reset');
const btn = document.querySelector('.btn');
const text = document.querySelector('.text');
const price = document.querySelector('#price');
const correctPrice = document.querySelector('.correct-price');

price.addEventListener('blur', () => {
    if (Number(price.value && price.value > 0)) {
        reset.style.opacity = '1';
        price.style.color = 'green';
        text.innerText = `Текущая цена: ${price.value}`;
        } else {
        price.style.borderColor = 'red';
        price.style.color = 'red';
        correctPrice.style.display = 'block';
        price.onclick = function () {
            correctPrice.style.display = 'none';
            price.style.borderColor = 'green';
            price.style.color = '';
        }
    }
});

btn.addEventListener('click', () => {
    reset.style.opacity = '0';
    price.value = '';
});


/*
Обработчик событий - это своего рода функционал, который при определенных действиях пользователя позволяет нам совершать
какие-то изменения в DOM
 */